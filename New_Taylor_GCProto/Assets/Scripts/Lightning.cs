﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
			if (Pointer_Control.pointerUp == true && RightPointer_Control.rightPointerUp == true && Middle_Control.middleUp == true && Ring_Control.ringUp == true && Pinky_Control.pinkyUp == false && RightMiddle_Control.rightMiddleUp == false && RightRing_Control.rightRingUp == false && RightPinky_Control.rightPinkyUp == true && Input.GetKeyDown(KeyCode.Space))
				gameObject.GetComponent<SpriteRenderer>().enabled = true;

			if (Pointer_Control.pointerUp == true && RightPointer_Control.rightPointerUp == true && Middle_Control.middleUp == false && Ring_Control.ringUp == false && Pinky_Control.pinkyUp == true && RightMiddle_Control.rightMiddleUp == true && RightRing_Control.rightRingUp == true && RightPinky_Control.rightPinkyUp == false && Input.GetKeyDown(KeyCode.Space))
				gameObject.GetComponent<SpriteRenderer>().enabled = true;

			if (gameObject.GetComponent<SpriteRenderer> ().enabled == true)
				StartCoroutine (WaitToReset ());



		}

		IEnumerator WaitToReset()
		{ 
			yield return new WaitForSeconds (2);
			gameObject.GetComponent<SpriteRenderer> ().enabled = false;
		}
		
	}
